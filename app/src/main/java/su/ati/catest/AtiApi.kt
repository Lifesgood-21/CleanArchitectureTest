package su.ati.catest

import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST


public interface AtiApi{

    @FormUrlEncoded
    @POST("")
    fun getCargoList(
            @Field("login") login: String,
            @Field("password") password: String ): Single<BaseResponse>

    @GET("")
    fun getCargoTypeList(): Single<BaseResponse>
}