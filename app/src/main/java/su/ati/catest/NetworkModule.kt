package su.ati.catest

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import java.security.SecureRandom
import javax.inject.Singleton
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.security.cert.CertificateException
import java.security.cert.X509Certificate

@Module
class NetworkModule(private val context: Context){


    private val baseUrl: String = "http://www.test.ru"

    @Provides
    @Singleton
    internal fun provideApi(retrofit: Retrofit): AtiApi {
        return retrofit.create(AtiApi::class.java)
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(client: OkHttpClient, gsonBuilder: GsonBuilder): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .client(client)
                .build()
    }

    @Provides
    @Singleton
    internal fun provideGsonBuilder(): GsonBuilder {
        return GsonBuilder()
                .serializeNulls()
                .setDateFormat("dd.MM.yyyy HH:mm:ss")
                .setLenient()
    }

    @Provides
    @Singleton
    internal fun provideHostNameVerifier(): HostnameVerifier{
        return HostnameVerifier(
            {hostname, _ -> hostname == baseUrl.replace("https://", "")}
        )
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(verifier: HostnameVerifier, socketFactory: SSLSocketFactory,
                                     trustManager: X509TrustManager): OkHttpClient {
        return OkHttpClient.Builder()
                //.sslSocketFactory(socketFactory, trustManager)
                .addNetworkInterceptor(StethoInterceptor())
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                //.hostnameVerifier(verifier)
                .build()
    }

    @Provides
    @Singleton
    internal fun provideSslSocketFactory(trustManager: X509TrustManager): SSLSocketFactory {
        val sslContext: SSLContext = SSLContext.getInstance("TLS")
        sslContext.init(null, arrayOf<TrustManager>(trustManager), SecureRandom())
        return sslContext.socketFactory
    }

    @Provides
    @Singleton
    internal fun provideTrustManager(): X509TrustManager {

        var trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
            }

            override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
            }

        })
        return trustAllCerts[0] as X509TrustManager
    }
}
