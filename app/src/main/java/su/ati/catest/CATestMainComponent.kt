package su.ati.catest

import dagger.Component
import su.ati.catest.view.MainActivity
import javax.inject.Singleton

@Component(
        dependencies = arrayOf(CATestAppComponent::class),
        modules = arrayOf(NetworkModule::class)
)
@MainScope
interface CATestMainComponent {
}

