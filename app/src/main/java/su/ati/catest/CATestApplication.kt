package su.ati.catest

import android.app.Application

class CATestApplication : Application() {

    companion object {
        @JvmStatic lateinit var graph: CATestAppComponent
        @JvmStatic lateinit var mainActivityGraph: CATestAppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initApplicationComponent()
    }

    private fun initApplicationComponent(){
        graph = DaggerCATestAppComponent.builder()
                .databaseModule(DatabaseModule(this))
                .networkModule(NetworkModule(this))
                .build()
    }
}