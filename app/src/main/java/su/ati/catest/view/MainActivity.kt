package su.ati.catest.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import su.ati.catest.AtiApi
import su.ati.catest.CATestApplication
import su.ati.catest.Cargo
import su.ati.catest.R
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainView {

    @Inject lateinit var api: AtiApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        CATestApplication.graph.inject(this)
    }

    override fun showCargoList(cargoList: List<Cargo>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onViewClick(view: View) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
