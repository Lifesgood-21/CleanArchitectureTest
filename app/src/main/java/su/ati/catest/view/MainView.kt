package su.ati.catest.view

import android.view.View
import su.ati.catest.Cargo

interface MainView {

    fun showCargoList(cargoList : List<Cargo>)
    fun onViewClick(view: View)
}