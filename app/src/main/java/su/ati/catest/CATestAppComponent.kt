package su.ati.catest

import dagger.Component
import su.ati.catest.view.MainActivity
import javax.inject.Singleton

@Component(modules = arrayOf(NetworkModule::class, DatabaseModule::class) )
@Singleton
interface CATestAppComponent{

    fun inject(mainActivity: MainActivity)

}

