package su.ati.catest

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule(private val context: Context){

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = context
}